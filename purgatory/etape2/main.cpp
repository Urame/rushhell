#include "Machine.hpp"

void	printAction(eAction act)
{
  if (act == MA)
    std::cout << "MOOVE APEND" << std::endl;  
  else if (act == HR)
    std::cout << "HALT RESET" << std::endl;
  else
    std::cout << "STATE_ERROR" << std::endl;
    
}

bool	toCompare(const std::string& reference, char *entree)
{
  int		i;
  eState	etat = S0;
  eState	etatTemp;
  int		alphaPos;
  bool		eq = false;

  for (i = 0; entree[i]; i++)
    {
      std::cout << "Token read " << entree[i] << std::endl;
      alphaPos = reference.find(entree[i]);
      etatTemp = gStateTable[etat][alphaPos];
      std::cout << "Next state = S" << etatTemp << std::endl;
      if (etatTemp == STATE_ERROR)
	  {
		  std::cout << "State error" << std::endl;
		  etat = S0;
	  }
	  else if (gActionTable[etat][alphaPos] != HR)
		{
			printAction(gActionTable[etat][alphaPos]);
			std::cout << "passage a l etat " << etatTemp << std::endl;
			etat = etatTemp;
			if (etat == S7)
				eq = true;
		}
    }
  return eq;
}


int	main(int ac, char **av)
{
  std::string alpha("mechant");
  if (ac > 1)
  {
	  if (toCompare(alpha, av[1]) == true)
		  std::cout << "The string is include" << std::endl;
	  else
		  std::cout << "The string isn't include" << std::endl;

  }
}
