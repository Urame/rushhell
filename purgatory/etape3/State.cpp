#include	"State.h"

State::State(const std::string & name, const bool & end)
	: name_(name),
	end_(end)
{
}

State::~State()
{
}
std::string	State::getName() const
{
	return name_;
}


bool	State::operator==(const State & state) const
{
	if (state.name_ == name_)
		return true;
	else
		return false;
}

bool	State::operator==(const std::string & str) const
{
	if (str == name_)
		return true;
	else
		return false;

}

void		State::AddLink(const Edge & edge, const std::string & state)
{
	std::map<Edge, std::string>::iterator	it;

	for (it = links_.begin(); it != links_.end() && !(it->first == edge); it++);
	if (it == links_.end())
	{
		links_.insert(std::pair<Edge, std::string>(edge, state));
	}
	else
	{
		links_.erase(it);
		links_.insert(std::pair<Edge, std::string>(edge, state));
	}
}

std::string	State::GetLink(const char &c)
{
	std::map<Edge, std::string>::iterator	it;

	for (it = links_.begin(); it != links_.end() && !(it->first == c); it++);
	if (it == links_.end())
		return "__init__";
	else
	{
		return (links_[c]);
	}
}
