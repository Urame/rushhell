#pragma	once
# include	"State.h"

class FSA
{
public:
	FSA();
	~FSA();
	void AddState(State *state);
	bool DefineInitialState(const std::string&);
	State*	operator[](const std::string &);
	std::string	getInitial() const;
private:
	std::vector<State*>	states_;
	std::string			initial_;
};
