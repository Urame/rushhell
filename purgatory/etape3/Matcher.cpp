#include	"Matcher.h"

Matcher::Matcher(FSA & fsa)
	: fsa_(fsa)
{

}

Matcher::~Matcher()
{
}

bool	Matcher::find(const std::string &str)
{
	std::string	curr_state = fsa_.getInitial();
	std::string	temp;
	size_t		count = 0;
	State		*state;

	for (size_t i = 0; i < str.size(); ++i)
	{
		if (fsa_[curr_state] != NULL)
			{
			temp = fsa_[curr_state]->GetLink(str[i]);
			if (temp == "__init__")
				curr_state = fsa_.getInitial();
			else if (temp == "__end__")
			{
				curr_state = fsa_.getInitial();
				count++;
			}
			else
			{
				curr_state = temp;
			}
		}
	}
	if (count >= 1)
		return	true;
	else
		return	false;
}
