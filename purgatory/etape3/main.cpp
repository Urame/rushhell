#include	<iostream>
#include	"Edge.h"
#include	"State.h"
#include	"FSA.h"
#include	"Matcher.h"


void	FillEdgeList(std::vector<Edge> & edge_list, const std::string & pass)
{

	for (size_t i = 0; i < pass.size(); i++)
	{
		edge_list.push_back(pass[i]);
	}
}

void	FillFSA(FSA & fsa, const std::vector<Edge> & edge_list)
{
	std::string	tab[] = { "S0", "S1", "S2", "S3", "S4", "S5", "S6", "__end__" };
	std::vector<std::string>			vec(tab, tab + sizeof(tab) / sizeof(std::string));
	std::vector<std::string>::iterator	it;
	State								*temp;
	std::vector<Edge>::const_iterator	it_edge = edge_list.begin();

	for (it = vec.begin(); it != vec.end(); ++it)
	{
		if (State::NewState(*it, temp) == true)
		{

			if ((it + 1) != vec.end())
			{
				temp->AddLink(*it_edge, *(it + 1));
				++it_edge;
			}
			fsa.AddState(temp);
		}
	}
	fsa.DefineInitialState("S0");
}




int main(int ac, char **av)
{
	std::string							pass("mechant");
	std::vector<Edge>					edge_list;
	FSA									fsa;
	Matcher								match(fsa);

	if (ac >= 2)
	{
		FillEdgeList(edge_list, pass);
		FillFSA(fsa, edge_list);
		if (match.find(av[1]) == true)
		{
			std::cout << "Include" << std::endl;
		}
		else
		{
			std::cout << "Not include" << std::endl;
		}
	}
}