#pragma	once
# include	"FSA.h"

class Matcher
{
public:
	Matcher(FSA	&fsa);
	~Matcher();
	bool	find(const std::string &);
private:
	FSA		&fsa_;
};
