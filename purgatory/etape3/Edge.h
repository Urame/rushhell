#pragma	once
#include <iostream>

class Edge
{
public:
	Edge(const char & c = '\0');
	~Edge();
	bool	operator==(const Edge &) const;
	bool	operator==(const char &) const;
	bool	operator()(const char &) const;
	bool	operator<(const Edge &) const;

	char	getc() const;

private:
	char	c_;
};
