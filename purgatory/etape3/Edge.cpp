#include "Edge.h"

Edge::Edge(const char & c)
	: c_(c)
{
}

Edge::~Edge()
{
}

bool	Edge::operator==(const Edge & edge) const
{
	if (edge.c_ == c_)
		return true;
	else
		return false;
}

bool	Edge::operator==(const char & edge) const
{
	if (edge == c_)
		return true;
	else
		return false;
}

bool	Edge::operator()(const char & c) const
{
	if (c == c_)
		return true;
	else
		return false;
}

char	Edge::getc() const
{
	return	c_;
}

bool	Edge::operator<(const Edge & e) const
{
	return (c_ < e.c_);
}