#include "FSA.h"
#include <iostream>

FSA::FSA()
{
}

FSA::~FSA()
{
	std::vector<State*>::iterator it;
	for (it = states_.begin(); it != states_.end(); it++)
	{
		delete *it;
	}
}
void	FSA::AddState(State *state)
{
	if (states_.size() == 0)
		initial_ = state->getName();
	states_.push_back(state);

}
bool FSA::DefineInitialState(const std::string & name)
{
	std::vector<State*>::iterator it;

	for (it = states_.begin(); it != states_.end() && !((*it)->getName() == name); it++);
	if (it == states_.end())
		return false;
	else
	{
		initial_ = name;
		return true;
	}
}

State*	FSA::operator[](const std::string &name)
{
	std::vector<State*>::iterator it = states_.begin();

	for (it = states_.begin(); it != states_.end() && !((*it)->getName() == name); it++);
	if (it == states_.end())
	{
		return NULL;
	}
	else
	{
		return	(*it);
	}
}
std::string	FSA::getInitial() const
{
	return	initial_;
}
