#pragma	once
# include <string>
# include <map>
# include <vector>
# include <algorithm>

# include "Edge.h"

class State
{
public:
	State(const std::string & name = "", const bool &end = false);
	~State();
	bool		operator==(const State &) const;
	bool		operator==(const std::string &) const;
	static bool	NewState(const std::string & name, State *& state)
	{
		static	bool						end_ = false;
		static	std::vector<std::string>	used_;
		std::vector<std::string>::iterator	it;

		if (end_ == true)
			return false;
		state = NULL;
		it = std::find(used_.begin(), used_.end(), name);
		if (it == used_.end())
		{
			used_.push_back(name);
			if (name == "__end__")
			{
				state = new State(name, true);
				end_ = true;
			}
			else
			{
				state = new State(name);
			}
			return true;
		}
		else
			return false;
	}

	void		AddLink(const Edge & edge, const std::string &state);
	std::string	getName() const;
	std::string	GetLink(const char &c);
private:
	std::string					name_;
	std::map<Edge, std::string>	links_;
	bool						end_;
};
