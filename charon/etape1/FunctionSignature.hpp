#ifndef _FUNCTIONSIGNATURE_HPP_
# define _FUNCTIONSIGNATURE_HPP_


#include <iostream>
#include <string>
#include <typeinfo>


template<typename Ty>
struct FunctionSignature;


template<typename Ret>
class FunctionSignature<Ret()>
{
	typedef	Ret (*type)();
};


template<typename Ret, typename Arg1>
struct FunctionSignature<Ret(Arg1)>
{
public:
	typedef	Ret(*type)(Arg1);
};


template<typename Ret, typename Arg1, typename Arg2>
struct FunctionSignature<Ret(Arg1, Arg2)>
{
public:
	typedef	Ret(*type)(Arg1, Arg2);
};


template<typename Ret, typename Arg1, typename Arg2, typename Arg3>
struct FunctionSignature<Ret(Arg1, Arg2, Arg3)>
{
public:
	typedef	Ret(*type)(Arg1, Arg2, Arg3);
};


template<typename Ret, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
struct FunctionSignature<Ret(Arg1, Arg2, Arg3, Arg4)>
{
public:
	typedef	Ret(*type)(Arg1, Arg2, Arg3, Arg4);
};

#endif /* !_FUNCTIONSIGNATURE_HPP_ */
