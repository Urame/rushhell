#ifndef		FUNCTION_H_
# define	FUNCTION_H_

template<typename T>
struct Function;


template<typename Ret>
class Function<Ret()>
{
	typedef	Ret(*Type_)();
private:
	class IFunctor_
	{
	public:
		virtual		~IFunctor_() {};
		virtual Ret	operator()() = 0;
	};
	class FunctorFptr_ : public IFunctor_
	{
	public:
		FunctorFptr_(Type_	const& pfunc)
		{
			func_ = pfunc;
		}
		virtual ~FunctorFptr_() {}
		virtual Ret	operator()(){
			return func_();
		}

	private:
		Type_	func_;
	};
	template<class C>
	class FunctorElse_ : public IFunctor_
	{

	public:
		FunctorElse_() {};
		FunctorElse_(C const& obj)
			: obj_(obj)
		{
		}
		virtual ~FunctorElse_() {};
		virtual Ret	operator()(){
			return obj_();
		}
	private:
		C	obj_;
	};

public:
	template<typename T>
	Function(T const & obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
	}
	Function(Type_ pfunc)
	{
		pfunc_ = new FunctorFptr_(pfunc);
	}
	template<typename T>
	Type_ operator=(T obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
		return obj;
	}
	template<typename T>
	Function(Function<T> const & obj)
	{
		pfunc_ = new FunctorElse_< Function<T> >(obj);
	}

	~Function()
	{
		delete	pfunc_;
	}
	Type_ operator=(Type_ pfunc)
	{
		pfunc_ = pfunc;
		return pfunc;
	}
	Ret	operator()()
	{
		return ((*pfunc_)());
	}
private:
	IFunctor_	*pfunc_;
};


template<typename Ret, typename Arg1>
struct Function<Ret(Arg1)>
{
	typedef	Ret(*Type_)(Arg1);
private:

	class IFunctor_
	{
	public:
		virtual		~IFunctor_() {};
		virtual Ret	operator()(Arg1 arg) = 0;
	};
	class FunctorFptr_ : public IFunctor_
	{
	public:
		FunctorFptr_(Type_	const& pfunc)
		{
			func_ = pfunc;
		}
		virtual ~FunctorFptr_() {}
		virtual Ret	operator()(Arg1 arg){
			return func_(arg);
		}

	private:
		Type_	func_;
	};
	template<class C>
	class FunctorElse_ : public IFunctor_
	{

	public:
		FunctorElse_() {};
		FunctorElse_(C const& obj)
			: obj_(obj)
		{
		}
		virtual ~FunctorElse_() {};
		virtual Ret	operator()(Arg1 arg){
			return obj_(arg);
		}
	private:
		C	obj_;
	};

public:
	template<typename T>
	Function(T const & obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
	}	
	Function(Type_ pfunc)
	{
		pfunc_ = new FunctorFptr_(pfunc);
	}
	template<typename T>
	Type_ operator=(T obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
		return obj;
	}
	template<typename T>
	Function(Function<T> const & obj)
	{
		pfunc_ = new FunctorElse_< Function<T> >(obj);
	}

	~Function()
	{
		delete	pfunc_;
	}
	Type_ operator=(Type_ pfunc)
	{
		pfunc_ = pfunc;
		return pfunc;
	}
	Ret	operator()(Arg1 arg1)
	{
		return ((*pfunc_)(arg1));
	}
private:
	IFunctor_	*pfunc_;
};


template<typename Ret, typename Arg1, typename Arg2>
struct Function<Ret(Arg1, Arg2)>
{
public:
	typedef	Ret(*Type_)(Arg1, Arg2);
private:
	class IFunctor_
	{
	public:
		virtual		~IFunctor_() {};
		virtual Ret	operator()(Arg1, Arg2) = 0;
	};
	class FunctorFptr_ : public IFunctor_
	{
	public:
		FunctorFptr_(Type_	const& pfunc)
		{
			func_ = pfunc;
		}
		virtual ~FunctorFptr_() {}
		virtual Ret	operator()(Arg1 arg1, Arg2 arg2){
			return func_(arg1, arg2);
		}

	private:
		Type_	func_;
	};
	template<class C>
	class FunctorElse_ : public IFunctor_
	{

	public:
		FunctorElse_() {};
		FunctorElse_(C const& obj)
			: obj_(obj)
		{
		}
		virtual ~FunctorElse_() {};
		virtual Ret	operator()(Arg1 arg1, Arg2 arg2){
			return obj_(arg1, arg2);
		}
	private:
		C	obj_;
	};

public:
	template<typename T>
	Function(T const & obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
	}
	Function(Type_ pfunc)
	{
		pfunc_ = new FunctorFptr_(pfunc);
	}
	template<typename T>
	Type_ operator=(T obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
		return obj;
	}
	template<typename T>
	Function(Function<T> const & obj)
	{
		pfunc_ = new FunctorElse_< Function<T> >(obj);
	}

	~Function()
	{
		delete	pfunc_;
	}
	Type_ operator=(Type_ pfunc)
	{
		pfunc_ = pfunc;
		return pfunc;
	}
	Ret	operator()(Arg1 arg1, Arg2 arg2)
	{
		return ((*pfunc_)(arg1, arg2));
	}
private:
	IFunctor_	*pfunc_;
};


template<typename Ret, typename Arg1, typename Arg2, typename Arg3>
struct Function<Ret(Arg1, Arg2, Arg3)>
{
public:
	typedef	Ret(*Type_)(Arg1, Arg2, Arg3);
private:
	class IFunctor_
	{
	public:
		virtual		~IFunctor_() {};
		virtual Ret	operator()(Arg1, Arg2, Arg3) = 0;
	};
	class FunctorFptr_ : public IFunctor_
	{
	public:
		FunctorFptr_(Type_	const& pfunc)
		{
			func_ = pfunc;
		}
		virtual ~FunctorFptr_() {}
		virtual Ret	operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3){
			return func_(arg1, arg2, arg3);
		}

	private:
		Type_	func_;
	};
	template<class C>
	class FunctorElse_ : public IFunctor_
	{

	public:
		FunctorElse_() {};
		FunctorElse_(C const& obj)
			: obj_(obj)
		{
		}
		virtual ~FunctorElse_() {};
		virtual Ret	operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3){
			return obj_(arg1, arg2, arg3);
		}
	private:
		C	obj_;
	};

public:
	template<typename T>
	Function(T const & obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
	}
	Function(Type_ pfunc)
	{
		pfunc_ = new FunctorFptr_(pfunc);
	}
	template<typename T>
	Function(Function<T> const & obj)
	{
		pfunc_ = new FunctorElse_< Function<T> >(obj);
	}

	~Function()
	{
		delete	pfunc_;
	}
	template<typename T>
	Type_ operator=(T obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
		return obj;
	}
	Type_ operator=(Type_ pfunc)
	{
		pfunc_ = pfunc;
		return pfunc;
	}
	Ret	operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3)
	{
		return ((*pfunc_)(arg1, arg2, arg3));
	}
private:
	IFunctor_	*pfunc_;
};


template<typename Ret, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
struct Function<Ret(Arg1, Arg2, Arg3, Arg4)>
{
public:
	typedef	Ret(*Type_)(Arg1, Arg2, Arg3, Arg4);
private:
	class IFunctor_
	{
	public:
		virtual		~IFunctor_() {};
		virtual Ret	operator()(Arg1, Arg2, Arg3, Arg4) = 0;
	};
	class FunctorFptr_ : public IFunctor_
	{
	public:
		FunctorFptr_(Type_	const& pfunc)
		{
			func_ = pfunc;
		}
		virtual ~FunctorFptr_() {}
		virtual Ret	operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3, Arg4 arg4){
			return func_(arg1, arg2, arg3, arg4);
		}

	private:
		Type_	func_;
	};
	template<class C>
	class FunctorElse_ : public IFunctor_
	{

	public:
		FunctorElse_() {};
		FunctorElse_(C const& obj)
			: obj_(obj)
		{
		}
		virtual ~FunctorElse_() {};
		virtual Ret	operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3, Arg4 arg4){
			return obj_(arg1, arg2, arg3, arg4);
		}
	private:
		C	obj_;
	};

public:
	template<typename T>
	Function(T const & obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
	}
	template<typename T>
	Function(Function<T> const & obj)
	{
		pfunc_ = new FunctorElse_< Function<T> >(obj);
	}
	Function(Type_ pfunc)
	{
		pfunc_ = new FunctorFptr_(pfunc);
	}
	~Function()
	{
		delete	pfunc_;
	}
	template<typename T>
	Type_ operator=(T obj)
	{
		pfunc_ = new FunctorElse_<T>(obj);
		return obj;
	}
	Type_ operator=(Type_ pfunc)
	{
		pfunc_ = pfunc;
		return pfunc;
	}
	Ret	operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3, Arg4 arg4)
	{
		return ((*pfunc_)(arg1, arg2, arg3, arg4));
	}
private:
	IFunctor_	*pfunc_;
};

#endif // !FUNCTION_H_
