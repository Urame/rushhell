#include	<iostream>
#include	<string>
#include	<typeinfo>
#include	<boost/bind.hpp>
#include	"Signature.h"
#include	"Function.h"

int	OneFunction(const std::string & str)
{
	std::cout << str << std::endl;
	return 1;
}

int	function1_2(char c){
	std::cout << c << std::endl;
	return	0;
}

int main()
{
		FunctionSignature<int(const std::string & str)>::type f = &OneFunction;
//		FunctionSignature<int(const std::string & str)>::type f2 = boost::bind(&OneFunction, _1);
		Function<int(const std::string & str)> f3 = &OneFunction;
		Function<int(const std::string & str)> f4 = boost::bind(&OneFunction, _1);

		std::cout << typeid( boost::bind(&OneFunction, _1)).name() << std::endl;
		f4("str");
		getchar();
	}